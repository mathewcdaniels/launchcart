package org.launchcode.launchcart;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.launchcart.data.CartRepository;
import org.launchcode.launchcart.data.ItemRepository;
import org.launchcode.launchcart.models.Cart;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by LaunchCode
 */
@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class CartControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Test
    public void testCanSaveItemToCartAndRedirect() throws Exception {
        Item item = new Item("Test Item", 5);
        itemRepository.save(item);
        mockMvc.perform(post("/cart/add-item/")
                .param("ids", Integer.toString(item.getUid())))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", "/cart"));
        mockMvc.perform(get("/cart/"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(item.getName())));
    }

    @Test
    public void testComputeTotal() {
        itemRepository.save(new Item("Test Item 1",1));
        itemRepository.save(new Item("Test Item 2",2));
        itemRepository.save(new Item("Test Item 3",3));
        itemRepository.save(new Item("Test Item 4",4));

        Cart cart = new Cart();
        List<Item> items = itemRepository.findAll();
        items.forEach(cart::addItem);
        assertEquals(10.0, cart.computeTotal(), .01);
    }

    @Test
    public void testTotalDisplayed() throws Exception {
        itemRepository.save(new Item("Test Item 1",1));
        itemRepository.save(new Item("Test Item 2",2));
        itemRepository.save(new Item("Test Item 3",3));
        itemRepository.save(new Item("Test Item 4",4));

        Cart cart = new Cart();
        List<Item> items = itemRepository.findAll();
        for (Item item: items) {
            cart.addItem(item);
        }
        cartRepository.save(cart);

        mockMvc.perform(get("/cart/")).andExpect(content().string(containsString("10.0")));
    }

    @Test
    public void testRemoveItemFromCart() {
        itemRepository.save(new Item("Test Item 1",1));
        itemRepository.save(new Item("Test Item 2",2));

        Cart cart = new Cart();
        List<Item> items = itemRepository.findAll();
        items.forEach(cart::addItem);
        cartRepository.save(cart);

        Item item = itemRepository.findOne(9);
        cart.removeItem(item);

        assertEquals(1, cart.getItems().size());

    }

    @Test
    public void testRemoveItemFromDisplay() throws Exception {
        itemRepository.save(new Item("Test Item 1",1));
        itemRepository.save(new Item("Test Item 2",2));

        Cart cart = new Cart();
        List<Item> items = itemRepository.findAll();
        for (Item item: items) {
            cart.addItem(item);
        }
        cartRepository.save(cart);

        Item item = itemRepository.findOne(12);
        cart.removeItem(item);

        mockMvc.perform(get("/cart/")).andExpect(content().string(not(containsString("Test Item 1"))));
    }
}
