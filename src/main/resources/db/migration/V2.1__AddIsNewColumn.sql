ALTER TABLE item ADD COLUMN is_new BOOLEAN;
UPDATE item SET is_new = TRUE;
ALTER TABLE item ALTER COLUMN is_new SET NOT NULL;
ALTER TABLE item ALTER COLUMN is_new SET DEFAULT TRUE;