CREATE TABLE public.cart (uid serial);
CREATE TABLE public.item (
    uid serial PRIMARY KEY,
    name VARCHAR(255),
    description VARCHAR(255),
    price DOUBLE PRECISION,
    cart_uid integer
    );
ALTER TABLE public.cart ADD CONSTRAINT unik_cart_id UNIQUE (uid);
ALTER TABLE public.item ADD CONSTRAINT fk_cart_id FOREIGN KEY (cart_uid) REFERENCES public.cart(uid);